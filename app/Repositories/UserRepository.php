<?php

namespace App\Repositories;

use App\User;
use Illuminate\Support\Facades\Auth;

class UserRepository
{
    /**
     * @param string $email
     * @return User
     */
    public function getUserByEmail(string $email)
    {
        return User::select(['id', 'password', 'token'])->where('email', $email)->first();
    }

    /**
     * @param array $data
     */
    public function createUser(array $data)
    {
        User::create($data);
    }

    /**
     * @param string $token
     * @return bool
     */
    public function authorizeByToken(string $token)
    {
        $user = $this->getUserByToken($token);
        if (!$user) {
            return false;
        }
        Auth::loginUsingId($user->id);

        return true;
    }

    /**
     * @param string $token
     * @return User
     */
    public function getUserByToken(string $token)
    {
        return User::where('token', $token)->first();
    }
}
