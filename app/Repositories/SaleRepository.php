<?php


namespace App\Repositories;

use App\Sale;
use Illuminate\Support\Collection;

class SaleRepository
{
    /**
     * @param int $offset
     * @param int $limit
     * @return Collection
     */
    public function getList(int $offset, int $limit)
    {
        return Sale::select(['id', 'date', 'client_id', 'user_id', 'car_id'])
            ->offset($offset)
            ->limit($limit)
            ->get();
    }

    /**
     * @return int
     */
    public function getTotalCount()
    {
        return Sale::count();
    }
}
