<?php

namespace App\Repositories;

use App\Client;
use Illuminate\Support\Collection;

class ClientRepository
{
    /**
     * @param int $offset
     * @param int $limit
     * @return Collection
     */
    public function getList(int $offset, int $limit)
    {
        return Client::select(['id', 'name', 'surname', 'middlename', 'phone'])
            ->offset($offset)
            ->limit($limit)
            ->get();
    }

    /**
     * @return int
     */
    public function getTotalCount()
    {
        return Client::count();
    }
}
