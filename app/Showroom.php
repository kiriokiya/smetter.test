<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Showroom extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'address'
    ];

    public function cars()
    {
        return $this->hasMany(Car::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
