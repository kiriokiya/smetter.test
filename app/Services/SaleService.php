<?php


namespace App\Services;

use App\Http\Response\BaseResponse;
use App\Http\Response\Model\FormatSale;
use App\Http\Response\Model\Paginate;
use App\Repositories\SaleRepository;
use App\Sale;

class SaleService
{
    /** @var SaleRepository  */
    private $repo;
    /** @var BaseResponse  */
    private $response;

    /**
     * SaleService constructor.
     * @param SaleRepository $repo
     * @param BaseResponse $response
     */
    public function __construct(SaleRepository $repo, BaseResponse $response)
    {
        $this->repo = $repo;
        $this->response = $response;
    }

    /**
     * @param array $data
     * @return BaseResponse
     */
    public function getSales(array $data): BaseResponse
    {
        list($page, $perPage) = $this->normalizePaginateInfo($data);
        $offset = ($page - 1) * $perPage;
        $sales = $this->repo->getList($offset, $perPage);
        $totalCount = $this->repo->getTotalCount();

        foreach ($sales as $sale) {
            $formatSale = new FormatSale();
            $formatSale->id = $sale->id;
            $formatSale->date = $sale->date;
            $formatSale->client->id = $sale->client->id;
            $formatSale->client->name = "{$sale->client->surname} {$sale->client->name} {$sale->client->middlename}";
            $formatSale->manager->id = $sale->user->id;
            $formatSale->manager->name = "{$sale->user->surname} {$sale->user->name} {$sale->user->middlename}";
            $formatSale->manager->showroom = $sale->user->showroom->address;
            $formatSale->car->id = $sale->car->id;
            $formatSale->car->vin = $sale->car->car_vin;
            $formatSale->car->model = "{$sale->car->carModel->name} {$sale->car->carModel->brand->name}";

            $this->response->data[] = $formatSale;
        }
        $this->response->paginate->fill($page, $perPage, count($this->response->data), $totalCount);

        return $this->response;
    }

    /**
     * @param array $data
     * @return array
     */
    private function normalizePaginateInfo(array $data): array
    {
        $page = $data['page'] ?? Paginate::DEFAULT_PAGE;
        $perPage = $data['perPage'] ?? Paginate::DEFAULT_PER_PAGE;

        return [$page, $perPage];
    }
}
