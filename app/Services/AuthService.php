<?php

namespace App\Services;

use App\Repositories\UserRepository;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthService
{
    /** @var UserRepository  */
    private $repo;

    /**
     * AuthService constructor.
     * @param UserRepository $repo
     */
    public function __construct(UserRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * @param array $data
     * @return User
     * @throws \Exception
     */
    public function getToken(array $data)
    {
        $user = $this->repo->getUserByEmail($data['email']);
        if (!$user || !Hash::check($data['password'] . $data['email'], $user->getAuthPassword())) {
            throw new \Exception('Wrong login or password');
        }

        return $user;
    }

    /**
     * @param array $data
     */
    public function registerUser(array $data)
    {
        $data['token'] = Str::random(32);
        $data['password'] = Hash::make($data['password'] . $data['email']);
        $this->repo->createUser($data);
    }
}
