<?php

namespace App\Services;

use App\Client;
use App\Http\Response\BaseResponse;
use App\Http\Response\Model\Paginate;
use App\Repositories\ClientRepository;

class ClientService
{
    /** @var ClientRepository  */
    private $repo;
    /** @var BaseResponse  */
    private $response;

    /**
     * ClientService constructor.
     * @param ClientRepository $repo
     * @param BaseResponse $response
     */
    public function __construct(ClientRepository $repo, BaseResponse $response)
    {
        $this->repo = $repo;
        $this->response = $response;
    }

    /**
     * @param array $data
     * @return BaseResponse
     */
    public function getClients(array $data): BaseResponse
    {
        list($page, $perPage) = $this->normalizePaginateInfo($data);
        $offset = ($page - 1) * $perPage;
        $this->response->data = $this->repo->getList($offset, $perPage);
        $totalCount = $this->repo->getTotalCount();
        $this->response->paginate->fill($page, $perPage, count($this->response->data), $totalCount);

        return $this->response;
    }

    /**
     * @param Client $client
     * @param array $data
     * @return BaseResponse
     */
    public function getClientBuys(Client $client, array $data): BaseResponse
    {
        list($page, $perPage) = $this->normalizePaginateInfo($data);
        $chunk = $client->sales->chunk($perPage);
        $this->response->data = $chunk[$page - 1] ?? [];
        $this->formatBuy();
        $this->response->paginate->fill($page, $perPage, count($this->response->data), count($client->sales));

        return $this->response;
    }

    /**
     * @param array $data
     * @return array
     */
    private function normalizePaginateInfo(array $data): array
    {
        $page = $data['page'] ?? Paginate::DEFAULT_PAGE;
        $perPage = $data['perPage'] ?? Paginate::DEFAULT_PER_PAGE;

        return [$page, $perPage];
    }

    /**
     *
     */
    private function formatBuy(): void
    {
        foreach ($this->response->data as $buy) {
            unset($buy->created_at, $buy->updated_at, $buy->deleted_at);
        }
    }
}
