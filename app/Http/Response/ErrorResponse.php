<?php

namespace App\Http\Response;

class ErrorResponse
{
    /** @var string */
    public $source;
    /** @var int  */
    public $code;
    /** @var string */
    public $detail;

    /**
     * ErrorResponse constructor.
     * @param int $code
     * @param string $detail
     * @param string $source
     */
    public function __construct(int $code, string $detail, string $source = '')
    {
        $this->source = $source;
        $this->code = $code;
        $this->detail = $detail;
    }
}
