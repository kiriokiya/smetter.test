<?php

namespace App\Http\Response;

use App\Http\Response\Model\Paginate;
use Illuminate\Support\Collection;

class BaseResponse
{
    /** @var Paginate  */
    public $paginate;
    /** @var Collection  */
    public $data;

    public function __construct(Paginate $paginate)
    {
        $this->paginate = $paginate;
    }
}
