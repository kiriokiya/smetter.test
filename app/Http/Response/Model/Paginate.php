<?php

namespace App\Http\Response\Model;

class Paginate
{
    public const DEFAULT_PAGE = 1;
    public const DEFAULT_PER_PAGE = 10;

    /** @var int */
    public $page;
    /** @var int */
    public $perPage;
    /** @var int */
    public $lastPage;
    /** @var int */
    public $totalCount;

    /**
     * @param int $page
     * @param int $perPage
     * @param int $realPerPage
     * @param int $totalCount
     */
    public function fill(int $page, int $perPage, int $realPerPage, int $totalCount)
    {
        $this->page = $page;
        $this->perPage = $realPerPage;
        $this->totalCount = $totalCount;
        $this->lastPage = ceil($totalCount / $perPage);
    }
}
