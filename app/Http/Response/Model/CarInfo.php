<?php

namespace App\Http\Response\Model;

class CarInfo
{
    /** @var int */
    public $id;
    /** @var string */
    public $vin;
    /** @var string */
    public $model;

}
