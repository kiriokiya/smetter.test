<?php

namespace App\Http\Response\Model;

use App\Http\Response\Model\CarInfo;
use App\Http\Response\Model\PeopleInfo;

class FormatSale
{
    public $id;
    public $date;
    public $client;
    public $manager;
    public $car;

    public function __construct()
    {
        $this->manager = new PeopleInfo();
        $this->client = new PeopleInfo();
        $this->car = new CarInfo();
    }
}
