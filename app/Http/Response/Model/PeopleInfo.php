<?php

namespace App\Http\Response\Model;

class PeopleInfo
{
    /** @var int */
    public $id;
    /** @var string */
    public $name;
}
