<?php

namespace App\Http\Middleware;

use App\Repositories\UserRepository;
use Closure;

class TokenAuthMiddleware
{
    /** @var UserRepository  */
    private $repo;

    public function __construct(UserRepository $repository)
    {
        $this->repo = $repository;
    }

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        $token = $request->headers->get('Authorization');
        if (!$token) {
            throw new \Exception('Missing Authorization header');
        }

        if (!$this->repo->authorizeByToken($token)) {
            throw new \Exception('Unknown token');
        }

        return $next($request);
    }
}
