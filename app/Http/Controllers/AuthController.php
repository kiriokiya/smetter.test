<?php

namespace App\Http\Controllers;

use App\Http\Requests\ApiAuthRequest;
use App\Http\Requests\RegistrationRequest;
use App\Http\Resources\AuthResource;
use App\Services\AuthService;

class AuthController extends Controller
{
    /**
     * @param ApiAuthRequest $request
     * @param AuthService $service
     * @return AuthResource
     * @throws \Exception
     */
    public function apiAuth(ApiAuthRequest $request, AuthService $service)
    {
        $data = $request->validated();
        $user = $service->getToken($data);

        return new AuthResource($user);
    }

    public function registration(RegistrationRequest $request, AuthService $service)
    {
        $data = $request->validated();
        $service->registerUser($data);
    }
}
