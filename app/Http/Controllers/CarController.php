<?php

namespace App\Http\Controllers;

use App\Car;
use App\Http\Middleware\TokenAuthMiddleware;
use App\Http\Requests\CreateCarRequest;
use App\Http\Resources\CarCreateResource;

class CarController extends Controller
{
    public function __construct()
    {
        $this->middleware(TokenAuthMiddleware::class);
    }

    /**
     * @param CreateCarRequest $request
     * @return CarCreateResource
     */
    public function create(CreateCarRequest $request)
    {
        $data = $request->validated();
        $client = Car::create($data);

        return new CarCreateResource($client);
    }
}
