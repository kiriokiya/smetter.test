<?php

namespace App\Http\Controllers;

use App\Client;
use App\Http\Middleware\TokenAuthMiddleware;
use App\Http\Requests\PaginateRequest;
use App\Http\Requests\CreateClientRequest;
use App\Http\Resources\BaseResource;
use App\Http\Resources\ClientCreateResource;
use App\Http\Resources\ClientViewResource;
use App\Services\ClientService;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware(TokenAuthMiddleware::class);
    }

    /**
     * @param CreateClientRequest $request
     * @return ClientCreateResource
     */
    public function create(CreateClientRequest $request)
    {
        $data = $request->validated();
        $client = Client::create($data);

        return new ClientCreateResource($client);
    }

    /**
     * @param PaginateRequest $request
     * @param ClientService $service
     * @return BaseResource
     */
    public function index(PaginateRequest $request, ClientService $service)
    {
        $data = $request->validated();
        $clients = $service->getClients($data);

        return new BaseResource($clients);
    }

    /**
     * @param Client $client
     * @return ClientViewResource
     */
    public function view(Client $client)
    {
        return new ClientViewResource($client);
    }

    /**
     * @param Client $client
     * @param PaginateRequest $request
     * @param ClientService $service
     * @return BaseResource
     */
    public function buys(Client $client, PaginateRequest $request, ClientService $service)
    {
        $data = $request->validated();
        $buys = $service->getClientBuys($client, $data);

        return new BaseResource($buys);
    }
}
