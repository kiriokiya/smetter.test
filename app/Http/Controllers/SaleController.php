<?php

namespace App\Http\Controllers;

use App\Http\Middleware\TokenAuthMiddleware;
use App\Http\Requests\CreateSaleRequest;
use App\Http\Requests\PaginateRequest;
use App\Http\Resources\BaseResource;
use App\Http\Resources\SaleViewResource;
use App\Sale;
use App\Http\Resources\SaleCreateResource;
use App\Services\SaleService;

class SaleController extends Controller
{
    public function __construct()
    {
        $this->middleware(TokenAuthMiddleware::class);
    }

    /**
     * @param CreateSaleRequest $request
     * @return SaleCreateResource
     */
    public function create(CreateSaleRequest $request)
    {
        $data = $request->validated();
        $sale = Sale::create($data);

        return new SaleCreateResource($sale);
    }

    /**
     * @param PaginateRequest $request
     * @param SaleService $service
     * @return BaseResource
     */
    public function index(PaginateRequest $request, SaleService $service)
    {
        $data = $request->validated();
        $sales = $service->getSales($data);

        return new BaseResource($sales);
    }

    /**
     * @param Sale $sale
     * @return SaleViewResource
     */
    public function view(Sale $sale)
    {
        return new SaleViewResource($sale);
    }
}
