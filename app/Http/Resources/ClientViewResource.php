<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientViewResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array|void
     */
    public function toArray($request)
    {
        return [
            'surname' => $this->surname,
            'name' => $this->name,
            'middlename' => $this->middlename,
            'phone' => $this->phone,
            'id' => $this->id,
        ];
    }
}
