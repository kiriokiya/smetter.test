<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SaleViewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'sale' => $this->id,
            'date' => $this->date,
            'client' => [
                'id' => $this->client->id,
                'name' => "{$this->client->surname} {$this->client->name} {$this->client->middlename}"
            ],
            'user' => [
                'id' => $this->user->id,
                'name' => "{$this->user->surname} {$this->user->name} {$this->user->middlename}",
                'showroom' => $this->user->showroom->address,
            ],
            'car' => [
                'id' => $this->id,
                'vin' => $this->car->car_vin,
                'model' => "{$this->car->carModel->name} {$this->car->carModel->brand->name}",
            ],
        ];
    }
}
