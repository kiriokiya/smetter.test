<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCarRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'car_vin' => 'required|string|min:1|max:50',
            'showroom_id' => 'required|exists:showrooms,id',
            'car_model_id' => 'required|exists:car_models,id',
        ];
    }
}
