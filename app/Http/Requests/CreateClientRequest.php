<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateClientRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'surname' => 'required|string|min:1|max:50',
            'name' => 'required|string|min:1|max:50',
            'middlename' => 'required|string|min:1|max:50',
            'phone' => 'required|string|min:1|max:20',
        ];
    }
}
