<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSaleRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date_format:Y-m-d|before:tomorrow',
            'client_id' => 'required|exists:clients,id',
            'user_id' => 'required|exists:users,id',
            'car_id' => 'required|exists:cars,id',
        ];
    }
}
