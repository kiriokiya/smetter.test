<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistrationRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|min:1|max:255',
            'password' => 'required|string|min:1|max:255',
            'surname' => 'required|string|min:1|max:50',
            'name' => 'required|string|min:1|max:50',
            'middlename' => 'required|string|min:1|max:50',
            'phone' => 'required|string|min:1|max:20',
            'showroom_id' => 'required|exists:showrooms,id',
        ];
    }
}
