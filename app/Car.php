<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Car extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'showroom_id', 'car_model_id', 'car_vin'
    ];

    public function carModel()
    {
        return $this->belongsTo(CarModel::class);
    }

    public function showroom()
    {
        return $this->belongsTo(Showroom::class);
    }

    public function sale()
    {
        return $this->belongsTo(Sale::class);
    }
}
