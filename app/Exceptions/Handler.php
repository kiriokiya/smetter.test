<?php

namespace App\Exceptions;

use App\Http\Response\ErrorResponse;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    public const UNDEFINED_API_EXCEPTION = 406;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Throwable
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof ValidationException) {
            $errors = $this->formatValidationException($exception);
            return response()->json(['errors'=> $errors], $exception->status);
        } else {
            $errors = new ErrorResponse(self::UNDEFINED_API_EXCEPTION, $exception->getMessage());
            return response()->json(['errors'=> $errors], self::UNDEFINED_API_EXCEPTION);
        }

    }

    /**
     * @param Throwable $exception
     * @return array
     */
    private function formatValidationException(Throwable $exception)
    {
        $errors = [];
        foreach ($exception->errors() as $field => $errorMessages) {
            foreach ($errorMessages as $errorMessage)
                $errors[] = new ErrorResponse($exception->status, $errorMessage, $field);
        }

        return $errors;
    }
}
