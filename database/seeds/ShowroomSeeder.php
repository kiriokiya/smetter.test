<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ShowroomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $showrooms = ['СПб, Пулковское ш., д.14',
            'СПб, Лиговский пр-т, д.213',
            'СПб, Каменноостровский пр-т, д.42'];

        foreach ($showrooms as $showroom) {
            DB::table('showrooms')->insert([
                'address' => $showroom,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
