<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class CarModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $models = [['X5', 1], ['X6', 1], ['X1', 1],
            ['CR-V', 2], ['Accord', 2], ['Beat', 2],
            ['Golf', 3], ['Polo', 3], ['Arteon', 3]];

        foreach ($models as $model) {
            DB::table('car_models')->insert([
                'name' => $model[0],
                'brand_id' => $model[1],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
