<p align="center">
    <h1 align="center">Smetter api</h1>
</p>


Как развернуть
-----

1. Скопируйте .env.example в .env
1. Укажите:<br>DB_CONNECTION <br>DB_HOST<br>DB_PORT<br> DB_DATABASE<br> DB_USERNAME<br> DB_PASSWORD
1. Выполните команду php artisan migrate
1. Выполните команду php artisan db:seed

Использование
-----

Для начала работы с системой необходимо зарегестрировать в системе нового пользователя, для этого используется метод **/api/registration**
### Регистрация
Type: POST

URI: /api/registration

Body:

```json
{
  "email": "email@email.com",
  "password": 1111,
  "surname": "surname",
  "name": "name",
  "middlename": "middlename",
  "phone": "989034567860",
  "showroom_id": 1
}
```

Response:

```json
{
  "data": {
    "id": 7
  }
}
```

В результате успешного выполнения будет создан новый пользователь и вы сможете войти в систему используя указнаный email и пароль. Для этого используется метод **/api/auth**
### Авторизация
Type: POST

URI: /api/auth

Body:

```json
{
  "email": "email@email.com",
  "password": 1111
}
```

Response:

```json
{
  "data": {
    "token": "hydldsaioJSDA1AKD"
  }
}
```

Полученный в результате токен необходимо передавать с другими запросами в заголовке **Authorization**

Методы api
-----

### Список клиентов
Type: GET

URI: /api/clients

Query: ?page=1&perPage=1

Response:

```json
{
    "data": [
        {
            "id": 1,
            "name": "Фамилия",
            "surname": "Имя",
            "middlename": "Отчество",
            "phone": "89345678761"
        }
    ],
    "meta": {
        "page": 1,
        "perPage": 1,
        "lastPage": 7,
        "totalCount": 7
    }
}
```

### Создание клиента
Type: POST

URI: /api/clients

Body:

```json
{
  "surname": "surname",
  "name": "name",
  "middlename": "middlename",
  "phone": "989034567867"
}
```

Response:

```json
{
    "data": {
        "id": 7
    }
}
```

### Информация о клиенте
Type: GET

URI: /api/clients/{id}

Response:

```json
{
    "data": {
        "surname": "surname",
        "name": "name",
        "middlename": "middlename",
        "phone": "989034567867",
        "id": 7
    }
}
```

### Покупки клиента
Type: GET

URI: /api/clients/{id}/buys

Query: ?page=1&perPage=10

Response:

```json
{
    "data": [
        {
            "id": 4,
            "date": "2021-06-01",
            "client_id": 7,
            "user_id": 1,
            "car_id": 1
        },
        {
            "id": 5,
            "date": "2021-05-01",
            "client_id": 7,
            "user_id": 1,
            "car_id": 3
        }
    ],
    "meta": {
        "page": 1,
        "perPage": 2,
        "lastPage": 1,
        "totalCount": 2
    }
}
```

### Список продаж
Type: GET

URI: /api/sales

Query: ?page=1&perPage=10

Response:

```json
{
    "data": [
        {
            "id": 1,
            "date": "2021-06-17",
            "client": {
                "id": 1,
                "name": "Имя Фамилия Отчество"
            },
            "manager": {
                "id": 1,
                "name": "s n m",
                "showroom": "Street"
            },
            "car": {
                "id": 1,
                "vin": "10-03",
                "model": "Hhh Volvo"
            }
        }
    ],
    "meta": {
        "page": 1,
        "perPage": 1,
        "lastPage": 6,
        "totalCount": 6
    }
}
```

### Создание продажи
Type: POST

URI: /api/sales

Body:

```json
{
  "date": "2021-06-16",
  "client_id": 1,
  "user_id": 1,
  "car_id": 2
}
```

Response:

```json
{
    "data": {
        "id": 6
    }
}
```

### Информация о продаже
Type: GET

URI: /api/sales/{id}

Response:

```json
{
    "data": {
        "sale": 4,
        "date": "2021-06-01",
        "client": {
            "id": 7,
            "name": "surname name middlename"
        },
        "user": {
            "id": 1,
            "name": "s n m",
            "showroom": "Street"
        },
        "car": {
            "id": 4,
            "vin": "10-03",
            "model": "Hhh Volvo"
        }
    }
}
```

### Добавление автомобиля
Type: POST

URI: /api/cars

Body:

```json
{
  "car_vin": "1111111",
  "showroom_id": 4,
  "car_model_id": 10
}
```

Response:

```json
{
    "data": {
        "id": 4
    }
}
```

### Ошибки

При использовании api вам могу вернуться два типа ошибок. 422 - ошибка валидации и 406 - для всех прочих ошибок.

Ошибка 422

Response:

```json
{
    "errors": [
        {
            "source": "date",
            "code": 422,
            "detail": "The date field is required."
        }
    ]
}
```

Ошибка 406

Response:

```json
{
    "errors": {
        "source": "",
        "code": 406,
        "detail": "No query results for model [App\\Sale] 8"
    }
}
```
