<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/clients', 'ClientController@create')->name('client.create');
Route::get('/clients', 'ClientController@index')->name('client.index');
Route::get('/clients/{client}', 'ClientController@view')->name('client.view');
Route::get('/clients/{client}/buys', 'ClientController@buys')->name('client.buys');

Route::post('/auth', 'AuthController@apiAuth')->name('auth.apiAuth');
Route::post('/registration', 'AuthController@registration')->name('auth.registration');

Route::post('/sales', 'SaleController@create')->name('sale.create');
Route::get('/sales', 'SaleController@index')->name('sale.index');
Route::get('/sales/{sale}', 'SaleController@view')->name('sale.view');

Route::post('/cars', 'CarController@create')->name('car.create');
